﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Castle.DynamicProxy.Generators.Emitters.SimpleAST;
using Microsoft.AspNetCore.Mvc.ViewComponents;
using Moq;
using SportsStore.Components;
using SportsStore.Models;
using Xunit;

namespace SportsStore.Tests
{
    public class NavigationMenuViewComponentTests
    {
        [Fact]
        public void Can_Select_Category()
        {
            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns((new Product[]
            {
                new Product {ProductId = 1, Name = "P1",Category = "Cat1"},
                new Product {ProductId = 2, Name = "P2",Category = "Cat2"},
                new Product {ProductId = 3, Name = "P3",Category = "Cat1"},
                new Product {ProductId = 4, Name = "P4",Category = "Cat2"},
                new Product {ProductId = 5, Name = "P5",Category = "Cat3"}
            }).AsQueryable<Product>());

            NavigationMenuViewComponent target=new NavigationMenuViewComponent(mock.Object);

            NavigationMenuResponceModel[] result = ((IEnumerable<NavigationMenuResponceModel>) (target.Invoke() as ViewViewComponentResult).ViewData.Model)
                .ToArray();

            Assert.True(Enumerable.SequenceEqual(
                new[]
                {
                    new NavigationMenuResponceModel{Category = "Cat1"},
                    new NavigationMenuResponceModel{Category = "Cat2"},
                    new NavigationMenuResponceModel{Category = "Cat3"}
                },result));
        }
    }
}
