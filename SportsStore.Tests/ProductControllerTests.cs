﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Moq;
using SportsStore.Controllers;
using SportsStore.Models;
using SportsStore.Models.ViewModels;
using Xunit;

namespace SportsStore.Tests
{
    public class ProductControllerTests
    {
        [Fact]
        public void Can_Filter_Products()
        {
            Mock<IProductRepository> mock=new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns((new Product[]
            {
                new Product {ProductId = 1, Name = "P1",Category = "Cat1"},
                new Product {ProductId = 2, Name = "P2",Category = "Cat2"},
                new Product {ProductId = 3, Name = "P3",Category = "Cat1"},
                new Product {ProductId = 4, Name = "P4",Category = "Cat2"},
                new Product {ProductId = 5, Name = "P5",Category = "Cat3"}
            }).AsQueryable<Product>());

            ProductController controller=new ProductController(mock.Object);
            controller.PageSize = 3;

            Product[] result = (controller.List("Cat2", 1).ViewData.Model as ProductListViewModel).Products.ToArray();

            Assert.Equal(2,result.Length);
            Assert.True(result[0].Name=="P2" && result[0].Category=="Cat2");
            Assert.True(result[1].Name=="P4" && result[1].Category=="Cat2");
        }
    }
}
