﻿using System.Linq;
using SportsStore.Models;
using Xunit;

namespace SportsStore.Tests
{
    public class CartTests
    {
        [Fact]
        public void Cart_Clean()
        {
            var p1 = new Product
            {
                ProductId = 1,
                Name = "P1",
                Price = 10
            };
            var p2 = new Product
            {
                ProductId = 2,
                Name = "P2",
                Price = 10
            };
            var p3 = new Product
            {
                ProductId = 3,
                Name = "P3",
                Price = 10
            };

            var cart = new Cart();
            cart.AddItem(p1, 2);
            cart.AddItem(p2, 2);
            cart.AddItem(p3, 2);

            cart.Clean();

            Assert.Equal(0,cart.Lines.Count());
        }
        [Fact]
        public void Calculate_Cart_Total()
        {
            var p1 = new Product
            {
                ProductId = 1,
                Name = "P1",
                Price = 10
            };
            var p2 = new Product
            {
                ProductId = 2,
                Name = "P2",
                Price = 10
            };
            var p3 = new Product
            {
                ProductId = 3,
                Name = "P3",
                Price = 10
            };

            var cart = new Cart();
            cart.AddItem(p1, 2);
            cart.AddItem(p2, 2);
            cart.AddItem(p3, 2);

            decimal result = cart.ComputeTotalValue();

            Assert.Equal(60,result);
        }

        [Fact]
        public void Can_Add_New_Lines()
        {
            var p1 = new Product
            {
                ProductId = 1, Name = "P1"
            };
            var p2 = new Product
            {
                ProductId = 2, Name = "P2"
            };

            var cart = new Cart();
            cart.AddItem(p1, 1);
            cart.AddItem(p2, 1);
            var result = cart.Lines.ToArray();

            Assert.Equal(2, result.Length);
            Assert.Equal(p1, result[0].Product);
            Assert.Equal(p2, result[1].Product);
        }

        [Fact]
        public void Can_Add_Quantity_For_Existing_Lines()
        {
            var p1 = new Product
            {
                ProductId = 1,
                Name = "P1"
            };
            var p2 = new Product
            {
                ProductId = 2,
                Name = "P2"
            };

            var cart = new Cart();
            cart.AddItem(p1, 1);
            cart.AddItem(p2, 1);
            cart.AddItem(p1, 10);
            var result = cart.Lines.ToArray();

            Assert.Equal(2, result.Length);
            Assert.Equal(11, result[0].Quantity);
            Assert.Equal(1, result[1].Quantity);
        }

        [Fact]
        public void Can_Remove_Line()
        {
            var p1 = new Product
            {
                ProductId = 1,
                Name = "P1"
            };
            var p2 = new Product
            {
                ProductId = 2,
                Name = "P2"
            };
            var p3 = new Product
            {
                ProductId = 3,
                Name = "P3"
            };

            var cart = new Cart();
            cart.AddItem(p1, 1);
            cart.AddItem(p2, 3);
            cart.AddItem(p3, 5);
            cart.AddItem(p2, 1);

            cart.RemoveLine(p2);

            Assert.Equal(0, cart.Lines.Count(e => e.Product == p2));
            Assert.Equal(2, cart.Lines.Count());
        }
    }
}