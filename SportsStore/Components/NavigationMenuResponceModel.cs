﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SportsStore.Components
{
    public class NavigationMenuResponceModel
    {
        public string DisplayCategory { get; set; }
        public string Category { get; set; }
    }
}
