﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace SportsStore.Models.Db
{
    public static class SeedData
    {
        public static void EnsurePopulated(IApplicationBuilder app)
        {
            ApplicationDbContext context = app.ApplicationServices.GetRequiredService<ApplicationDbContext>();
            context.Database.Migrate();
            if (!context.Products.Any())
            {
                context.Products.AddRange(
                    new Product{Name="Каяк",Description = "Лодка на одного человека.", CategoryDesciption = "Водный спорт",Category = "Water_Sport",Price = 275},
                    new Product { Name = "Спасательный жилет", Description = "Защита жизни", CategoryDesciption = "Водный спорт", Category = "Water_Sport", Price = 48.95m },
                    new Product { Name = "Футбольный мяч", Description = "Мяч по стандарту FIFA", CategoryDesciption = "Футбол", Category = "Football", Price =  19.5m },
                    new Product { Name = "Угловой флаг", Description = "Профессиональные реквизиты", CategoryDesciption = "Футбол", Category = "Football", Price =  34.95m },
                    new Product { Name = "Стадион", Description = "Стадион на 36000 посетителей", CategoryDesciption = "Футбол", Category = "Football",Price = 79500 },
                    new Product { Name = "Умная шапка", Description = "Увеличивает эфективность мозга на 75%", CategoryDesciption = "Шахматы", Category = "Chess", Price = 16 },
                    new Product { Name = "Неустойчивый стул", Description = "Деморализует противника", CategoryDesciption = "Шахматы", Category = "Chess", Price = 39.95m },
                    new Product { Name = "Шахматная доска", Description = "Доска для игр в шахматы.", CategoryDesciption = "Шахматы", Category = "Chess", Price = 75 },
                    new Product { Name = "Король", Description = "Шахматная фигура", CategoryDesciption = "Король", Category = "King", Price = 1200 }
                    );
                context.SaveChanges();
            }
        }
    }
}
