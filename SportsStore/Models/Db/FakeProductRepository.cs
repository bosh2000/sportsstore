﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SportsStore.Models
{
    public class FakeProductRepository : IProductRepository
    {
        public IQueryable<Product> Products => new List<Product> {
            new Product{Name="Product1",Price=10},
            new Product{Name="Product2",Price=20},
            new Product{Name="Product3",Price=30},
            new Product{Name="Product4",Price=40},
            new Product{Name="Product5",Price=50},
            new Product{Name="Product6",Price=60}
        }.AsQueryable<Product>();
    }
}
