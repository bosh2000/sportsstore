using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using SportsStore.Models;
using SportsStore.Models.Db;

namespace SportsStore
{
    public class Startup
    {
        public Startup(IConfiguration cfg)
        {
            configuration = cfg;
        }

        public IConfiguration configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(configuration["Data:SportStoreProducts:ConnectionString"]));
            services.AddTransient<IProductRepository, EFProductRepository>();
            services.AddMvc(mvcOptions => mvcOptions.EnableEndpointRouting = false);
            services.AddMemoryCache();
            services.AddSession();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment()) app.UseDeveloperExceptionPage();
            app.UseStatusCodePages();
            app.UseStaticFiles();
            app.UseSession();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    null,
                    "{category}/Page{productPage:int}",
                    new {controller = "Product", action = "List"});
                routes.MapRoute(
                    null,
                    "Page{productPage:int}",
                    new {Controller = "Product", action = "List", productPage = 1});
                routes.MapRoute(
                    null,
                    "{category}",
                    new {controller = "Product", action = "List", productPage = 1});
                routes.MapRoute(
                    null,
                    "",
                    new {controller = "Product", action = "List", productPage = 1});
                routes.MapRoute(
                    "default",
                    "{controller=Product}/{action=List}/{id?}");
            });
            SeedData.EnsurePopulated(app);
        }
    }
}